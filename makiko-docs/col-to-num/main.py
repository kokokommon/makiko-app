import string

def col2num(col):
    num = 0
    for c in col:
        if c in string.ascii_letters:
            num = num * 26 + (ord(c.upper()) - ord('A')) + 1
    return num

sections = [['V','D','AI','AC','AM','P','B','I','Y'],
['S','W','L','N','G','AA','X','M','F'],
['AG','AL','O','AH','AJ','AK','AB'],
['T','U','Q','C','AF','J'],
['AD','K','R','H','Z','AE','E'],
['AB','AG','O','H','AH','K','B','Y','Z','X','AD','AL','E','P','T','R','M','D']]

new_sections = []

for section in sections:
    section_col_numbers = []
    for column in section:
        col_number = col2num(column) - 1
        section_col_numbers.append(col_number)
    new_sections.append(section_col_numbers)

print (new_sections)

import csv

with open("output.csv", "w") as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerows(new_sections)