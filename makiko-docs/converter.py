import csv
import json

questions_list = []
filename = "Goldsmith新訳Final_v_drikus"

def handle_single_option(response_option):
    new_response_option = { 'key': None, 'value': None }
    response_option = response_option.split(':')
    if (len(response_option) > 1):
        new_response_option['key'] = response_option[0]
        new_response_option['value'] = response_option[1]
    else:
        new_response_option['value'] = response_option[0]

    return new_response_option


def handle_response_type_options(response_type_obj, existing_response_options):
    response_options = []
    if response_type_obj == 'Agreement Scale':
        response_type = 'slider'
    elif (response_type_obj == 'radio_extra'):
        response_type = 'radio_extra'
    else:
        response_type = 'radio'

    response_options_list = existing_response_options.split(';')
    for option in response_options_list:
        option = handle_single_option(option)
        response_options.append(option)
        
    return response_type, response_options

def parse_row(row):
    q_id = row[0]
    question_en = row[1]
    question_jp = row[2]
    response_type = row[3]
    existing_response_options = row[5]
    response_type, response_options = handle_response_type_options(response_type, existing_response_options)
    try:
        pos_neg = bool( int(row[4]) )
        pass
    except Exception as identifier:
        pos_neg = None
        pass

    question_obj = {
        "q_id": q_id,
        "question_en": question_en,
        "question_jp": question_jp,
        "response_type": response_type,
        "response_options": response_options,
        "pos_neg": pos_neg
    }

    questions_list.append(question_obj)

def open_file():
    with open(filename + '.csv') as csvfile:
        questionreader = csv.reader(csvfile, delimiter='\t', quotechar='"')
        next(questionreader, None)
        for row in questionreader:
            parse_row(row)

open_file()

# question_json = json.dumps([ob for ob in questions_list])

with open(filename + '.json', 'w') as outfile:
    json.dump([ob for ob in questions_list], outfile)