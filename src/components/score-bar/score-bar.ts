import { Component, Input } from '@angular/core';

/**
 * Generated class for the ScoreBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'score-bar',
  templateUrl: 'score-bar.html'
})
export class ScoreBarComponent {

  @Input('score') score;
  @Input('maxScore') maxScore;

  constructor() {
    console.log('Hello ScoreBarComponent Component');
  }

}
