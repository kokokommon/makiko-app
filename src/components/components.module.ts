import { NgModule } from '@angular/core';
import { ScoreBarComponent } from './score-bar/score-bar';
@NgModule({
	declarations: [ScoreBarComponent],
	imports: [],
	exports: [ScoreBarComponent]
})
export class ComponentsModule {}
