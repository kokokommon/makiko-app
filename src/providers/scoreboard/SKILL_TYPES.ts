export const SKILL_TYPES = [
    {
        name: 'F1 (Active Engagement)',
        question_ids: ['21','3','34','28','38','15','1','8','24']
    },
    {
        name: 'F2 (Perceptual Abilities)',
        question_ids: ['18','22','11','13','6','26','23','12','5']
    },
    {
        name: 'F3 (Musical Training)',
        question_ids: ['32','37','14','33','35','36','27']
    },
    {
        name: 'F4 (Emotions)',
        question_ids: ['19','20','16','2','31','9']
    },
    {
        name: 'F5 (Singing Abilities)',
        question_ids: ['29','10','17','7','25','30','4']
    },
    {
        name: 'FG (General Sophistication)',
        question_ids: ['27','32','14','7','33','10','1','24','25','23','29','37','4','15','19','17','12','3']
    },
]

					
