import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { QuestionProvider } from '../question/question.provider';
import { AnswerProvider } from '../answer/answer.provider';
import { SKILL_TYPES } from './SKILL_TYPES';

/*
  Generated class for the ScoreboardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScoreboardProvider {

  constructor(public http: Http, private questionProvider: QuestionProvider, private answerProvider: AnswerProvider
  ) {
    console.log('Hello ScoreboardProvider Provider');
  }

  calcRealAnswer(answerValue, posNeg) {
    let realAnswer = (answerValue - 4) * -2;
    realAnswer = answerValue += realAnswer;
  }

  calcQuestionScore(question, answer) {
    console.log(question.q_id, answer.value);

    const answerInt = parseInt(answer.value);
    let score = answerInt;
    if (question.pos_neg === true) {

    }
    else if (question.pos_neg === false) {
      score += (answerInt - 4) * -2;
    } else {
      score = 0;
    }
    
    console.log(question.q_id, '*', question.pos_neg, '=', score);
    return score;
  }

  calcScorePerType(questions, answers) {
    let score = 0;
    
    answers.map(answer => {
      try {
        const question = questions.find(question => {
          return question.q_id === answer.q_id;
        })
        if (question) {
          score += this.calcQuestionScore(question, answer);
        }
      } catch (error) {
        console.error(error);
      }
    })
    return score;
  }

  calcScore() {

    const answers = this.answerProvider.answers;

    let scores = SKILL_TYPES.map(skillType => {
      let name = skillType.name;
      const questions = this.questionProvider.questions.filter(question => {
        return skillType.question_ids.includes(question.q_id);
      });
      const maxScore = questions.length * 7;
      console.log(skillType.name, skillType.question_ids, questions);
      let score = this.calcScorePerType(questions, answers);
      return { name, score, maxScore };
    })

    console.log("SCORE: ", scores);

    return scores;
  }

}
