import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { QUESTIONS } from './QUESTIONS';
import { Question } from './question';
import { ENV } from '@app/env';

/*
  Generated class for the QuestionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QuestionProvider {

  questions: Question[];

  currentIndex: number;

  constructor(public http: Http) {

    if (ENV.mode === 'Development') {
      this.questions = QUESTIONS.slice(30, 35);
    } else {
      this.questions = QUESTIONS;
    }

  }

  setCurrentIndex(idx) {
    this.currentIndex = idx;
  }

}
