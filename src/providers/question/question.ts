
export enum EnumResponseType {
  RADIO = "radio",
  RADIO_EXTRA = "radio_extra",
  SLIDER = "slider"
}

export interface Question {
  q_id: string;
  question_en: string,
  question_jp: string,
  response_type: EnumResponseType,
  response_options: any[],
  pos_neg: boolean;
  optional?: boolean;
}