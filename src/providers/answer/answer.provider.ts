import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Answer } from './answer';

import { ResultsApiProvider } from '../results-api/results-api';

/*
  Generated class for the AnswerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnswerProvider { 

  answers: Answer[] = [];

  constructor(public resultsApi: ResultsApiProvider) {
    console.log('Hello AnswerProvider Provider');
  }

  getAnswer(q_id) {
    return this.answers.find(answer => {
      return answer.q_id === q_id;
    })
  }

  updateAnswer(answer) {
    const existingAnswerIindex = this.answers.findIndex((answerItem, i) => {
      return answerItem === answer || answerItem.q_id === answer.q_id;
    })

    if (existingAnswerIindex > -1) {
      this.answers[existingAnswerIindex] = answer;
    } else {
      this.answers.push(answer);
    }
    console.log(answer);
  }

  postAnswers(): Promise<any> {

    console.log(this.answers);

    let submitDate = new Date().toString();

    let results = [];
    
    results.push('submit date');
    results.push(submitDate);

    results = results.concat(this.answers);

    return this.resultsApi.postResultsArray(results);
  }

}
