export class TimeCollection {
  startTime: number;
  answerTime: number;
  endTime: number;

  constructor(startTime, answerTime, endTime) {
    this.startTime = startTime;
    this.answerTime = answerTime;
    this.endTime = endTime;
  }
}

export class Answer {
  q_id: string;
  value: string;
  label: string;
  times: TimeCollection[];

  constructor(q_id, value, label, times) {
    this.q_id = q_id;
    this.value = value;
    this.label = label;
    this.times = times;
  }
} 