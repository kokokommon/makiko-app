import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ENV } from '@app/env';

/*
  Generated class for the ResultsApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResultsApiProvider {

  private url: string;
  private apiKey: string;

  constructor(public http: Http) {
    console.log('Hello ResultsApiProvider Provider');
    
    this.url = ENV.apiUrl || 'https://secret-lake-63033.herokuapp.com/api/sheets';
    this.apiKey = ENV.apiKey || '123';
  }
  
  private setDefaultHttpHeaders(headers: Headers): void {
    headers.append('Authorization', this.apiKey);
  }

  public postResultsArray(results): Promise<any> {

    const headers = new Headers();
    this.setDefaultHttpHeaders(headers);

    return this.http.post(
      this.url,
      results,
      {
        headers
      }
    ).toPromise();
  }

}
