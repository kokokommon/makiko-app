import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  items = ["Email", "Project", "Developer"]

  constructor(public navCtrl: NavController) {

  }

}
