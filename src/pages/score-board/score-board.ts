import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ScoreboardProvider } from '../../providers/scoreboard/scoreboard.provider';
import { QuestionnairePage } from '../questionnaire/questionnaire';

/**
 * Generated class for the ScoreBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-score-board',
  templateUrl: 'score-board.html',
})
export class ScoreBoardPage {

  scores: any;
  homeButtonText: string = '戻る';

  constructor(public navCtrl: NavController, public navParams: NavParams, private scoreboardProvider: ScoreboardProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScoreBoardPage');
    this.scores = this.scoreboardProvider.calcScore();
  }

  gotoHome() {
    this.navCtrl.push(QuestionnairePage);
  }

}
