import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { Question, EnumResponseType } from '../../providers/question/question';
import { QuestionProvider } from '../../providers/question/question.provider';

import { Answer, TimeCollection } from '../../providers/answer/answer';
import { AnswerProvider } from '../../providers/answer/answer.provider';

import { ScoreBoardPage } from '../score-board/score-board';

/**
 * Generated class for the QuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html',
})
export class QuestionPage {

  nextQuestionText: string = '次へ進む';
  answerText: string = '';

  questionIndex: number;
  question: Question;

  selectedRadio: any;
  customAnswer: any;

  answer: Answer;

  startTime: number;
  answerTime: number;
  endTime: number;

  constructor
  (
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private questionProvider: QuestionProvider,
    private answerProvider: AnswerProvider,
    public loadingCtrl: LoadingController
  ) 
  {
    this.questionIndex = navParams.get('questionIndex');
    this.question = this.questionProvider.questions[this.questionIndex];
    this.checkIfAnswered();
  }

  ionViewDidEnter() {
    this.setStartTime();
  }

  checkIfAnswered() {
    console.log(this.question);
    const existingAnswer = this.answerProvider.getAnswer(this.question.q_id);

    if (existingAnswer) {
      this.answer = existingAnswer;
    } else {
      this.answer = new Answer(this.question.q_id, null, '', []);
    }
  }

  pickAnswer(value: any) {
    this.answerTime = this.getTime();
    this.answer.value = value;
    this.updateAnswer(this.answer)
  }

  private checkCustom(existingValue) {
    if (
      this.question.response_type === EnumResponseType.RADIO_EXTRA && 
      this.selectedRadio === this.question.response_options.length.toString() &&
      this.customAnswer
    ) {
      try {
        const combinedAnswer = this.selectedRadio + ' - ' + this.customAnswer;
        return combinedAnswer
      } catch (error) {
        
      }
    } else {
      return existingValue;
    }
  }

  updateAnswer(answer: Answer) {
    const timeCollection = new TimeCollection(this.startTime, this.answerTime, this.endTime);
    answer.value = this.checkCustom(answer.value);
    answer.times = [timeCollection];
    this.answerProvider.updateAnswer(answer);
  }

  logForm(form) {
    console.log(form);
  }

  previousQuestion() {
    this.navCtrl.push(QuestionPage, {
      questionIndex: this.questionIndex - 1
    })
  }

  nextQuestion() {
    this.endTime = this.getTime();
    this.updateAnswer(this.answer);
    this.navCtrl.push(QuestionPage, {
      questionIndex: this.questionIndex + 1
    })
  }

  private submitAgain() {
    let alert = this.alertCtrl.create({
      title: '何かが間違っていた',
      message: 'あなたの答えをもう一度提出してください。',
      buttons: [
        {
          // cancel
          text: '操作を取り消す',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          // try again
          text: '再試行する',
          handler: () => {
            this.submitQuestionnaire();
            console.log('Try again clicked');
          }
        }
      ]
    });
    alert.present();
  }

  finishQuestionnaire() {
    this.endTime = this.getTime();
    this.updateAnswer(this.answer);
    this.submitQuestionnaire();
  }

  submitQuestionnaire() {

    let loading = this.loadingCtrl.create({
      content: '回答が提出されている間お待ちください。'
    });

    loading.present();

    this.answerProvider.postAnswers().then(value => {
      loading.dismiss();
      console.log("yeah", value);
      this.navCtrl.push(ScoreBoardPage, {
        questionIndex: this.questionIndex + 1
      })
    }).catch(error => {
      loading.dismiss();
      console.error("damn", error);
      this.submitAgain();
    })
  }

  getTime() {
    const d = new Date();
    const t = d.getTime();
    return t;
  }

  setStartTime() {
    this.startTime = this.getTime();
  }

}
