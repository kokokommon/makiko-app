import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { QuestionnairePage } from '../pages/questionnaire/questionnaire';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QuestionProvider } from '../providers/question/question.provider';
import { AnswerProvider } from '../providers/answer/answer.provider';
import { ScoreBoardPage } from '../pages/score-board/score-board';
import { ScoreboardProvider } from '../providers/scoreboard/scoreboard.provider';
import { ResultsApiProvider } from '../providers/results-api/results-api';
import { QuestionPageModule } from '../pages/question/question.module';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    QuestionnairePage,
    ScoreBoardPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    QuestionPageModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    QuestionnairePage,
    ScoreBoardPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QuestionProvider,
    AnswerProvider,
    ScoreboardProvider,
    ResultsApiProvider
  ]
})
export class AppModule {}
