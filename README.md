This is the repository for the Makiko App!

## How to run this application

### Install dependencies

Make sure you have node.js installed.

Using your terminal of choice, navigate to the repository folder and run the following commands:

```bash
$ sudo npm install -g ionic cordova
$ sudo npm install
```

### Run application

#### Browser

Then, to run it in the browser, run the following command:

```bash
$ ionic serve
```

#### IOS

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

#### Android

```bash
$ ionic cordova platform add android
$ ionic cordova run android
```

#### Bundle application for web

##### Using environment variables for development / staging
```bash
$ npm run build
```

##### Using environment variables for production
```bash
$ npm run build --prod
```